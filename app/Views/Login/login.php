<h3><?= $title ?></h3>
<form action="/login/check">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="form-group">
        <label>Käyttäjätunnus</label>
        <input class="form-control" name="user" placeholder="Syötä käyttäjätunnus" maxlenght="30">
    </div>
    <div class="form-group">
        <label>Salasana</label>
        <input class="form-control" name="password" type="password" placeholder="Syötä salasana" maxlenght="30">
    </div>
    <button class="btn btn-success">Kirjaudu</button>
    <?= anchor('login/register','Rekisteröidy') ?>
</form>