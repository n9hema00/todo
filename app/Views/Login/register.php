<h3><?= $title ?></h3>
<form action="/login/registration">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="form-group">
        <label>Käyttäjätunnus</label>
        <input class="form-control" name="username" placeholder="Syötä käyttäjätunnus" maxlenght="30">
    </div>
    <div class="form-group">
        <label>Etunimi</label>
        <input class="form-control" name="firstname" placeholder="Syötä etunimi" maxlenght="30">
    </div>
    <div class="form-group">
        <label>Sukunimi</label>
        <input class="form-control" name="lastname" placeholder="Syötä sukunimi" maxlenght="30">
    </div>
    <div class="form-group">
        <label>Salasana</label>
        <input class="form-control" name="password" type="password" placeholder="Syötä salasana" maxlenght="30">
    </div>
    <div class="form-group">
        <label>Salasana uudestaan</label>
        <input class="form-control" name="password2" type="password" placeholder="Syötä salasana uudelleen" maxlenght="30">
    </div>
    <button class="btn btn-success">Rekisteröidy</button>
</form>