<h3><?= $title ?></h3>
<form action="/todo/create">
    <div class="cols-12">
    <?= Config\Services::validation()->listErrors(); ?>
    <div class="form-group">
        <label>Otsikko</label>
        <input class="form-control" name="title" placeholder="Lisää otsikko" maxlength="255">
    </div>
    <div class="form-group">
        <label>Kuvaus</label>
        <textarea class="form-control" name="description" placeholder="Lisää tehtävän kuvaus..." rows="5" cols="50"></textarea>
    </div>
    <button class="btn btn-success">Tallenna</button>
</form>