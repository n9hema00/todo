<h3><?= $title ?></h3>
<?= anchor('todo/create','Lisää uusi tehtävä')?>
<table class="table">
    <tr>
        <th>Otsikko</th>
        <th>Käyttäjä</th>
        <th>Kuvaus</th>
        <th></th>
    </tr>
<?php foreach ($todos as $todo): ?>
    <tr>
        <td><?= $todo['title'] ?></td>
        <td><?= $todo['firstname'] . ' ' . $todo['lastname'] ?></td>
        <td><?= $todo['description'] ?></td>
        <td><?= anchor('todo/delete/' . $todo['id'], 'delete')?></td>
    </tr>
<?php endforeach; ?>
</table>